# Подключение проекта к проверке в Sonarqube
- Импортировать проект в [sonarqube](https://sonarqube.tripleap.ru/projects).
- Создать в корневой папке проекта файл **sonar-project.properties** и указать в нем по крайней мере один параметр
```
sonar.projectKey=my_projectKey
```
*projectKey* можно взять во вкладке **Project Information** в настройках проекта.
- Добавить переменные окружения для проекта `SONAR_TOKEN` и `SONAR_HOST_URL`.
- Добавить в файл **.gitlab-ci.yml** проверку
```
sonarqube-check:
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
    GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - sonar-scanner -Dsonar.qualitygate.wait=true
  allow_failure: true
  rules:
    - if: $CI_COMMIT_REF_NAME == 'main' || $CI_PIPELINE_SOURCE == 'merge_request_event'
```
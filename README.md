# skillbox-diploma

Код шаблонного микросервиса в качестве примера для обучения. Инфраструктура для сервиса находится в репозитории [Infra](https://gitlab.com/svkoval67/infra).

## Запуск локальных тестов
```
docker compose up -d
docker compose run tests
docker compose run fmt
docker compose down
```

## Нагрузочное тестирование
```
locust -f locustfile.py --host=https://dev.tripleap.ru
```
